import "./Body.css";
import React, { Component } from "react";
import { Range } from "react-range";
import { Link } from "react-router-dom";

export default class Body extends Component {
  state = { height: [100], weight: [85], age: [35], size: [44.5] };
  render() {
    console.log(this.state.height, this.state.weight);
    return (
      <>
        <div className="body1">
          <div className="containerf">
            <h3 className="digisize">DigiSize.</h3>
            <div className="row mt-1">
              <div className="col-lg-6">
                <div className="mini-container">
                  <div className="info1">
                    <h4 className="mt-lg-5">Enter your body measurements</h4>
                    <div className="d-flex align-items-center justify-content-between mb-2 info">
                      <p>Height</p>
                      <p>{this.state.height[0]} cm</p>
                    </div>
                    <Range
                      className="nmadur"
                      step={1}
                      min={0}
                      max={200}
                      values={this.state.height}
                      onChange={height => this.setState({ height })}
                      renderTrack={({ props, children }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "11px",
                            width: "100%",
                            background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                              this.state.height[0] / 2
                            }%,  #E5E5E5 ${this.state.height[0] / 2}%)`,
                            borderRadius: "5px",
                          }}
                        >
                          {children}
                        </div>
                      )}
                      renderThumb={({ props }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "29px",
                            width: "29px",
                            backgroundColor: "#fff",
                            borderRadius: "50%",
                            border: "1px solid #135CFB",
                            outline: "none",
                          }}
                        />
                      )}
                    />
                  </div>

                  <div className="info1">
                    <div className="d-flex align-items-center justify-content-between mb-2 info">
                      <p>Weight</p>
                      <p>{this.state.weight[0]} kg</p>
                    </div>
                    <Range
                      className="nmadur"
                      step={1}
                      min={0}
                      max={245}
                      values={this.state.weight}
                      onChange={weight => this.setState({ weight })}
                      renderTrack={({ props, children }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "11px",
                            width: "100%",
                            // backgroundColor: "red"
                            background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                              this.state.weight[0] / 2.4
                            }%,  #E5E5E5 ${this.state.weight[0] / 2.4}%)`,
                            borderRadius: "5px",
                          }}
                        >
                          {children}
                        </div>
                      )}
                      renderThumb={({ props }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "29px",
                            width: "29px",
                            backgroundColor: "#fff",
                            borderRadius: "50%",
                            border: "1px solid #135CFB",
                            outline: "none",
                          }}
                        />
                      )}
                    />
                  </div>

                  <div className="info1">
                    <div className="d-flex align-items-center justify-content-between mb-2 info">
                      <p>Age</p>
                      <p>{this.state.age[0]} yrs</p>
                    </div>
                    <Range
                      className="nmadur"
                      step={1}
                      min={0}
                      max={100}
                      values={this.state.age}
                      onChange={age => this.setState({ age })}
                      renderTrack={({ props, children }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "11px",
                            width: "100%",
                            // backgroundColor: "red"
                            background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                              this.state.age[0] / 1
                            }%,  #E5E5E5 ${this.state.age[0] / 1}%)`,
                            borderRadius: "5px",
                          }}
                        >
                          {children}
                        </div>
                      )}
                      renderThumb={({ props }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "29px",
                            width: "29px",
                            backgroundColor: "#fff",
                            borderRadius: "50%",
                            border: "1px solid #135CFB",
                            outline: "none",
                          }}
                        />
                      )}
                    />
                  </div>

                  <div className="info1">
                    <div className="d-flex align-items-center justify-content-between mb-2 info">
                      <p>Shoe size</p>
                      <p>{this.state.size[0]} EU</p>
                    </div>
                    <Range
                      className="nmadur"
                      step={1}
                      min={0}
                      max={100}
                      values={this.state.size}
                      onChange={size => this.setState({ size })}
                      renderTrack={({ props, children }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "11px",
                            width: "100%",
                            // backgroundColor: "red"
                            background: `linear-gradient(to right, #135CFB 0%, #135CFB ${this.state.size[0]}%,  #E5E5E5 ${this.state.size[0]}%)`,
                            borderRadius: "5px",
                          }}
                        >
                          {children}
                        </div>
                      )}
                      renderThumb={({ props }) => (
                        <div
                          {...props}
                          style={{
                            ...props.style,
                            height: "29px",
                            width: "29px",
                            backgroundColor: "#fff",
                            borderRadius: "50%",
                            border: "1px solid #135CFB",
                            outline: "none",
                          }}
                        />
                      )}
                    />
                  </div>
                  <div className="save">
                    <div className="row mb-5">
                      <div className="col-lg-12 col-md-6">
                        <h3 className="mb-3">
                          {this.state.height[0] < 180 &&
                          this.state.height[0] > 150 &&
                          this.state.weight[0] > 80 &&
                          this.state.weight[0] < 100
                            ? "Looks just like me! "
                            : null}
                        </h3>
                      </div>
                      <div className="col-lg-12 col-md-6">
                        <Link
                          to="/home"
                          className="btn btn-primary save1 d-inline-block"
                        >
                          Save and see my sizes
                        </Link>
                      </div>
                    </div>
                    <p className="readOur">
                      Read our <a href="#"> Terms & Conditions</a> and{" "}
                      <a href="#"> Privacy Policy</a>
                    </p>
                  </div>
                </div>
              </div>

              <div className="col-lg-6">
                <div className="right-side">
                  <div className="body-tabs">
                    <div class="tab-content" id="myTabContent">
                      <div
                        class="tab-pane fade show active"
                        id="home"
                        role="tabpanel"
                        aria-labelledby="home-tab"
                      >
                        <img
                          style={{
                            transform: ` ${
                              `scale(` +
                              (this.state.weight[0] * 0.99) / 80 +
                              `,` +
                              (this.state.height[0] * 0.8) / 150 +
                              `)`
                            }`,
                          }}
                          src="/images/body/Vector.svg"
                          alt="man"
                        />
                      </div>
                      <div
                        class="tab-pane fade"
                        id="profile"
                        role="tabpanel"
                        aria-labelledby="profile-tab"
                      >
                        <img
                          style={{
                            transform: ` ${
                              `scale(` +
                              (this.state.weight[0] * 0.99) / 80 +
                              `,` +
                              (this.state.height[0] * 0.8) / 150 +
                              `)`
                            }`,
                          }}
                          src="/images/body/women.png"
                          alt="man"
                        />
                      </div>
                    </div>
                    <ul class="nav nav-tabs w-50" id="myTab" role="tablist">
                      <li class="nav-item">
                        <a
                          class="nav-link active"
                          id="home-tab"
                          data-toggle="tab"
                          href="#home"
                          role="tab"
                          aria-controls="home"
                          aria-selected="true"
                        >
                          <img src="/images/body/image 2.png" alt="" />
                        </a>
                      </li>
                      <li class="nav-item">
                        <a
                          class="nav-link"
                          id="profile-tab"
                          data-toggle="tab"
                          href="#profile"
                          role="tab"
                          aria-controls="profile"
                          aria-selected="false"
                        >
                          <img src="/images/body/image 3.png" alt="" />
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  }
}
