import React from 'react'
import Navbar from '../Navbar'

export default function Settings(props) {
    return (
        <div>
            <Navbar activeMenu={props.activeMenu}/>
            <div className="container">
                <h2>Settings</h2>
            </div>
        </div>
    )
}
