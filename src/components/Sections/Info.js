import React from 'react'
import Navbar from '../Navbar'

export default function Info(props) {
    return (
        <div>
            <Navbar activeMenu={props.activeMenu}/>
            <div className="container">
                <h2>Info</h2>
            </div>
        </div>
    )
}
