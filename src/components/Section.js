import "../index.css";
import "./Section.css";
import React, { useState } from "react";
import { Link } from "react-router-dom";
function Section() {
  const [enter, setEnter] = useState(false);
  const [invalid, setInvalid] = useState("s");
  const [person, setPerson] = useState([
    {
      id: 1,
      name: "Abduvali",
      email: "nazirov@gmail.com",
    },
  ]);

  const enterSite = () => {
    let nameInp = document.querySelector(".inutName");
    let emailInp = document.querySelector(".inutEmail");
    person.map(item => {
      if (item.name === nameInp.value && item.email === emailInp.value) {
        setEnter(true);
        setInvalid(true);
      } else {
        setInvalid(false);
        setEnter(false);
      }
    });
  };
  console.log(person);
  return (
    <section className="section-1">
      <div className="containerf">
        <h2>DigiSize.</h2>
        <div className="row">
          <div className="col-lg-6">
            <div className="container1">
              <h3>Create an account</h3>
              <button className="btn1 btn btn-block">
                <img
                  className="mr-2"
                  src="/images/section/google-icon.svg"
                  alt=""
                />
                Sign up with Google
              </button>
              <div className="d-flex align-items-center div-2">
                <hr />
                <p>OR</p>
                <hr />
              </div>
              <p className={`invalidEnter ${!invalid ? "d-block" : "d-none"}`}>
                Invalid Name or Email !
              </p>
              <label className="w-100">
                Name
                <input type="text" className="form-control inutName mb-3" />
              </label>
              <label className="w-100">
                Email
                <input type="email" className="form-control inutEmail" />
              </label>
              <Link
                to={enter ? "/body" : "/"}
                onClick={() => enterSite()}
                className={`btn ${
                  enter ? "btn-primary" : "btn-success"
                } btn-block mt-3 mb-3`}
              >
                {!enter ? "Check and Enter" : "Enter"}
                {/* Kirish */}
              </Link>
              <a className="link1" href="#">
                Mobile version
              </a>
              <h6 className="mb-5">
                Already have an account?
                <a href="#"> Sign in</a>
              </h6>
            </div>
          </div>
          <div className="col-lg-6 second-part text-center">
            <div className="container2">
              <div>
                <div
                  id="carouselExampleIndicators"
                  class="carousel slide slide1"
                  data-ride="carousel"
                >
                  <ol class="carousel-indicators">
                    <li
                      data-target="#carouselExampleIndicators"
                      data-slide-to="0"
                      class="active"
                    ></li>
                    <li
                      data-target="#carouselExampleIndicators"
                      data-slide-to="1"
                    ></li>
                    <li
                      data-target="#carouselExampleIndicators"
                      data-slide-to="2"
                    ></li>
                  </ol>
                  <div class="carousel-inner">
                    <div class="carousel-item active">
                      <img
                        class="d-block w-100"
                        src="/images/section/shop.svg"
                        alt="First slide"
                      />
                      <h3>Faster Shopping</h3>
                      <p>
                        Just find something you like. Never have a doubt in your
                        mind about which size to pick. Using your DigiSize
                        avatar, we'll do it for you!
                      </p>
                    </div>
                    <div class="carousel-item">
                      <img
                        class="d-block w-100"
                        src="/images/section/shop.svg"
                        alt="First slide"
                      />
                      <h3>Faster Shopping</h3>
                      <p>
                        Just find something you like. Never have a doubt in your
                        mind about which size to pick. Using your DigiSize
                        avatar, we'll do it for you!
                      </p>
                    </div>
                    <div class="carousel-item">
                      <img
                        class="d-block w-100"
                        src="/images/section/shop.svg"
                        alt="First slide"
                      />
                      <h3>Faster Shopping</h3>
                      <p>
                        Just find something you like. Never have a doubt in your
                        mind about which size to pick. Using your DigiSize
                        avatar, we'll do it for you!
                      </p>
                    </div>
                  </div>
                  <a
                    class="carousel-control-prev"
                    href="#carouselExampleIndicators"
                    role="button"
                    data-slide="prev"
                  >
                    <span
                      class="carousel-control-prev-icon"
                      aria-hidden="true"
                    ></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a
                    class="carousel-control-next"
                    href="#carouselExampleIndicators"
                    role="button"
                    data-slide="next"
                  >
                    <span
                      class="carousel-control-next-icon"
                      aria-hidden="true"
                    ></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default Section;
