import { Link } from 'react-router-dom';
import './Navbar.css'
function Navbar(props) {
  return (
    <div className="navbar00">
      <div className="nav2 d-flex align-items-center justify-content-between">
        <div className="brand">
          <Link to="/home">
            <h2>DigiSize.</h2>
          </Link>
        </div>
        <nav>
          <div onClick={props.activeMenu} className="logo">
            <img className="ml-auto" src="/images/menu.svg" alt="rasm" />
          </div>
        </nav>
      </div>
    </div>
  );
}

export default Navbar;
