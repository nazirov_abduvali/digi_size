import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Section from "./components/Section";
import Body from "./components/Sections/Body";
import SideBar from "./components/Sections/Sidebar";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/">
          <Section />
        </Route>
        <Route path="/home" render={() => <SideBar />} />
        <Route path="/body" render={() => <Body />} />
      </Switch>
    </Router>
  );
}

export default App;
