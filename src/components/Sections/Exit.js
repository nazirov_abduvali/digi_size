import React from "react";
import Navbar from "../Navbar";

export default function Exit(props) {
    const {activeMenu} = props;
  return (
    <div>
      <Navbar activeMenu={activeMenu}/>
      <div className="container">
        <h2>Exit</h2>
        
      </div>
    </div>
  );
}
