import "./Sidebar.css";
import React, { Component } from "react";
import { Range } from "react-range";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import {
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
  Row,
  Col,
  Container,
  Modal,
  ModalBody,
} from "reactstrap";
import classnames from "classnames";
import Brands from "./Brands";
import Settings from "./Settings";
import Info from "./Info";
import Exit from "./Exit";
class SideBar extends Component {
  constructor() {
    super();
    this.state = {
      status: true,
      class1: "activePage",
      pageInfo: "home",
      modal: false,
      height: [100],
      weight: [85],
      age: [35],
      size: [44.5],
      activeTab: "1",
      openMenu: true,
    };
  }

  toggle = () => {
    this.setState({ modal: !this.state.modal });
  };
  toggle2 = tab => {
    if (this.state.activeTab !== tab) {
      this.setState({ activeTab: tab });
    }
  };
  activeChange = status => {
    this.setState({ pageInfo: status });
    console.log(this.state.pageInfo, this.state.status);
  };

  render() {
    const activeMenu = () => {
      this.setState({
        openMenu: !openMenu,
      });
    };

    const { class1, pageInfo, activeTab, openMenu } = this.state;
    const { className, data } = this.props;
    const { toggle, toggle2 } = this;
    console.log(data);
    return (
      <Router>
        <div className="sideBar">
          <div
            className={openMenu ? "left-site openMenu" : "left-site closeMenu"}
          >
            <Container className="h-100">
              <Row className="h-100">
                <Col md={12} className="h-100 ">
                  <div className="d-flex align-items-center justify-content-between">
                    <div className="brand">
                      <Link to="/home">
                        <h2>DigiSize.</h2>
                      </Link>
                    </div>
                  </div>
                  <div className="box1 d-flex align-items-start flex-column">
                    <ul className="left-menu mb-auto">
                      <li
                        className={pageInfo === "home" ? class1 : null}
                        onClick={() => this.activeChange("home")}
                      >
                        <Link to="/home" className="d-flex align-items-center">
                          <span className="icon icon-home"></span>
                          <p className="m-0">Home</p>
                        </Link>
                      </li>
                      <li
                        className={pageInfo === "settings" ? class1 : null}
                        onClick={() => this.activeChange("settings")}
                      >
                        <Link
                          to="/settings"
                          className="d-flex align-items-center"
                        >
                          <span className="icon icon-settings"></span>
                          <p className="m-0">Settings</p>
                        </Link>
                      </li>
                      <li
                        className={pageInfo === "info" ? class1 : null}
                        onClick={() => this.activeChange("info")}
                      >
                        <Link to="/info" className="d-flex align-items-center">
                          <span className="icon icon-info"></span>
                          <p className="m-0">Terms & privacy</p>
                        </Link>
                      </li>
                      <li
                        className={pageInfo === "exit" ? class1 : null}
                        onClick={() => this.activeChange("exit")}
                      >
                        <Link to="/exit" className="d-flex align-items-center">
                          <span className="icon icon-exit"></span>
                          <p className="m-0">Log out</p>
                        </Link>
                      </li>
                    </ul>
                    <button
                      onClick={this.toggle}
                      className="btn btn-primary btn-block mt-auto"
                    >
                      Edit my measures
                    </button>
                  </div>
                </Col>
              </Row>
            </Container>
          </div>
          <div
            className={openMenu ? "right-site openNav" : "right-site closeNav"}
          >
            <Switch>
              <Route exact path="/home">
                <Brands activeMenu={activeMenu}/>
              </Route>
              <Route path="/settings">
                <Settings activeMenu={activeMenu}/>
              </Route>
              <Route path="/info">
                <Info activeMenu={activeMenu}/>
              </Route>
              <Route  path="/exit">
                <Exit activeMenu={activeMenu}/>
              </Route>
            </Switch>
          </div>
        </div>
        <div className="modal101">
          <Modal
            size="lg"
            isOpen={this.state.modal}
            toggle={toggle}
            className={className}
          >
            <ModalBody>
              <div className="roww row">
                <div className="col-lg-6">
                  <div className="mini-container">
                    <div className="info1">
                      <h4 className="mt-lg-3">Enter your body measurements</h4>
                      <div className="d-flex align-items-center justify-content-between mb-2 info">
                        <p>Height</p>
                        <p>{this.state.height[0]} cm</p>
                      </div>
                      <Range
                        className="nmadur"
                        step={1}
                        min={0}
                        max={200}
                        values={this.state.height}
                        onChange={height => this.setState({ height })}
                        renderTrack={({ props, children }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "11px",
                              width: "100%",
                              // backgroundColor: "red"
                              background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                                this.state.height[0] / 2
                              }%,  #E5E5E5 ${this.state.height[0] / 2}%)`,
                              borderRadius: "5px",
                            }}
                          >
                            {children}
                          </div>
                        )}
                        renderThumb={({ props }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "29px",
                              width: "29px",
                              backgroundColor: "#fff",
                              borderRadius: "50%",
                              border: "1px solid #135CFB",
                              outline: "none",
                            }}
                          />
                        )}
                      />
                    </div>

                    <div className="info1">
                      <div className="d-flex align-items-center justify-content-between mb-2 info">
                        <p>Weight</p>
                        <p>{this.state.weight[0]} kg</p>
                      </div>
                      <Range
                        className="nmadur"
                        step={1}
                        min={0}
                        max={245}
                        values={this.state.weight}
                        onChange={weight => this.setState({ weight })}
                        renderTrack={({ props, children }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "11px",
                              width: "100%",
                              // backgroundColor: "red"
                              background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                                this.state.weight[0] / 2.4
                              }%,  #E5E5E5 ${this.state.weight[0] / 2.4}%)`,
                              borderRadius: "5px",
                            }}
                          >
                            {children}
                          </div>
                        )}
                        renderThumb={({ props }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "29px",
                              width: "29px",
                              backgroundColor: "#fff",
                              borderRadius: "50%",
                              border: "1px solid #135CFB",
                              outline: "none",
                            }}
                          />
                        )}
                      />
                    </div>

                    <div className="info1">
                      <div className="d-flex align-items-center justify-content-between mb-2 info">
                        <p>Age</p>
                        <p>{this.state.age[0]} yrs</p>
                      </div>
                      <Range
                        className="nmadur"
                        step={1}
                        min={0}
                        max={100}
                        values={this.state.age}
                        onChange={age => this.setState({ age })}
                        renderTrack={({ props, children }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "11px",
                              width: "100%",
                              // backgroundColor: "red"
                              background: `linear-gradient(to right, #135CFB 0%, #135CFB ${
                                this.state.age[0] / 1
                              }%,  #E5E5E5 ${this.state.age[0] / 1}%)`,
                              borderRadius: "5px",
                            }}
                          >
                            {children}
                          </div>
                        )}
                        renderThumb={({ props }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "29px",
                              width: "29px",
                              backgroundColor: "#fff",
                              borderRadius: "50%",
                              border: "1px solid #135CFB",
                              outline: "none",
                            }}
                          />
                        )}
                      />
                    </div>

                    <div className="info1">
                      <div className="d-flex align-items-center justify-content-between mb-2 info">
                        <p>Shoe size</p>
                        <p>{this.state.size[0]} EU</p>
                      </div>
                      <Range
                        className="nmadur"
                        step={1}
                        min={0}
                        max={100}
                        values={this.state.size}
                        onChange={size => this.setState({ size })}
                        renderTrack={({ props, children }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "11px",
                              width: "100%",
                              // backgroundColor: "red"
                              background: `linear-gradient(to right, #135CFB 0%, #135CFB ${this.state.size[0]}%,  #E5E5E5 ${this.state.size[0]}%)`,
                              borderRadius: "5px",
                            }}
                          >
                            {children}
                          </div>
                        )}
                        renderThumb={({ props }) => (
                          <div
                            {...props}
                            style={{
                              ...props.style,
                              height: "29px",
                              width: "29px",
                              backgroundColor: "#fff",
                              borderRadius: "50%",
                              border: "1px solid #135CFB",
                              outline: "none",
                            }}
                          />
                        )}
                      />
                    </div>
                    <div className="save">
                      <button onClick={toggle} className="btn btn-primary save">
                        Save and see my sizes
                      </button>
                      <p className="readOur">
                        Read our <a href="#"> Terms & Conditions</a> and{" "}
                        <a href="#"> Privacy Policy</a>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="right-side">
                    <div className="body-tabs">
                      <Nav className="" tabs>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: activeTab === "1",
                            })}
                            onClick={() => {
                              toggle2("1");
                            }}
                          >
                            <img src="/images/body/image 2.png" alt="" />
                          </NavLink>
                        </NavItem>
                        <NavItem>
                          <NavLink
                            className={classnames({
                              active: activeTab === "2",
                            })}
                            onClick={() => {
                              toggle2("2");
                            }}
                          >
                            <img src="/images/body/image 3.png" alt="" />
                          </NavLink>
                        </NavItem>
                      </Nav>
                      <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">
                          <img
                            style={{
                              transform: ` ${
                                `scale(` +
                                (this.state.weight[0] * 0.75) / 80 +
                                `,` +
                                (this.state.height[0] * 0.75) / 150 +
                                `)`
                              }`,
                            }}
                            src="/images/body/Vector.svg"
                            alt="man"
                          />
                        </TabPane>
                        <TabPane tabId="2">
                          <img
                            style={{
                              transform: ` ${
                                `scale(` +
                                (this.state.weight[0] * 0.75) / 80 +
                                `,` +
                                (this.state.height[0] * 0.75) / 150 +
                                `)`
                              }`,
                            }}
                            src="/images/body/women.png"
                            alt="man"
                          />
                        </TabPane>
                      </TabContent>
                    </div>
                  </div>
                </div>
              </div>
            </ModalBody>
          </Modal>
        </div>
      </Router>
    );
  }
}

SideBar.propTypes = {};

export default SideBar;
