import React, { useState } from "react";
import {
  Modal,
  ModalBody,
  TabContent,
  TabPane,
  Nav,
  NavItem,
  NavLink,
} from "reactstrap";
import classnames from "classnames";
import "./Brands.css";
import { Link } from "react-router-dom";
import Navbar from "../Navbar";

export default function Brands(props) {
  const { buttonLabel, className, activeMenu, details } = props;
  const [activeTab2, setActiveTab2] = useState("9");
  const [activeTab, setActiveTab] = useState("3");
  const [activeElement, setActiveElement] = useState({
    id: 1,
    img: "/images/brands/1.png",
    text: "Perfect size: M",
  });

  const elements = [
    { id: 1, img: "/images/brands/1.png", text: "Perfect size: M" },
    { id: 2, img: "/images/brands/2.png", text: "Perfect size: M" },
    { id: 3, img: "/images/brands/3.png", text: "Perfect size: M" },
    { id: 4, img: "/images/brands/4.png", text: "Perfect size: M" },
    { id: 5, img: "/images/brands/5.png", text: "Perfect size: M" },
    { id: 6, img: "/images/brands/6.png", text: "Perfect size: M" },
  ];
  console.log(elements);
  const [modal, setModal] = useState(false);
  console.log(details);

  const toggle = tab => {
    if (tab < 6) {
      if (activeTab != tab) setActiveTab(tab);
    } else if (tab > 5) {
      if (activeTab2 != tab) setActiveTab2(tab);
    } else {
      setModal(!modal);
      setActiveElement(tab);
    }
  };
  return (
    <>
      <div className="brands">
        <Navbar activeMenu={activeMenu} />
        <div className="containert text-center">
          <div className="form">
            <input
              type="search"
              className=" form-control"
              placeholder="Search"
            />
          </div>
          <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item ml-auto">
              <a
                class="nav-link "
                id="pills-home-tab"
                data-toggle="pill"
                href="#pills-home"
                role="tab"
                aria-controls="pills-home"
                aria-selected="true"
              >
                Skirt
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="pills-profile-tab"
                data-toggle="pill"
                href="#pills-profile"
                role="tab"
                aria-controls="pills-profile"
                aria-selected="false"
              >
                Shoes
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="pills-contact-tab"
                data-toggle="pill"
                href="#pills-contact"
                role="tab"
                aria-controls="pills-contact"
                aria-selected="false"
              >
                Shirt
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link active"
                id="pills-contact-tab"
                data-toggle="pill"
                href="#pills-contact1"
                role="tab"
                aria-controls="pills-contact1"
                aria-selected="false"
              >
                T-Shirt
              </a>
            </li>
            <li class="nav-item ">
              <a
                class="nav-link "
                id="pills-contact-tab"
                data-toggle="pill"
                href="#pills-contact2"
                role="tab"
                aria-controls="pills-contact2"
                aria-selected="false"
              >
                Trousers
              </a>
            </li>
            <li class="nav-item">
              <a
                class="nav-link"
                id="pills-contact-tab"
                data-toggle="pill"
                href="#pills-contact3"
                role="tab"
                aria-controls="pills-contact3"
                aria-selected="false"
              >
                Dress
              </a>
            </li>
            <li class="nav-item mr-auto">
              <a
                class="nav-link"
                id="pills-contact-tab"
                data-toggle="pill"
                href="#pills-contact4"
                role="tab"
                aria-controls="pills-contact4"
                aria-selected="false"
              >
                Combo
              </a>
            </li>
          </ul>
          <div class="tab-content" id="pills-tabContent">
            <div
              class="tab-pane fade"
              id="pills-home"
              role="tabpanel"
              aria-labelledby="pills-home-tab"
            >
              <h3>Your Skirt sizes</h3>
            </div>
            <div
              class="tab-pane fade"
              id="pills-profile"
              role="tabpanel"
              aria-labelledby="pills-profile-tab"
            >
              <h3>Your Shoes sizes</h3>
            </div>
            <div
              class="tab-pane fade"
              id="pills-contact"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <h3>Your Shirt sizes</h3>
            </div>
            <div
              class="tab-pane fade  show active"
              id="pills-contact1"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <h3>Your T-shirt sizes</h3>
              <div className="row">
                {elements &&
                  elements.map((item, index) => (
                    <div className="col-lg-4 col-sm-6 col-12">
                      <div className="card">
                        <div className="box3 d-flex align-items-center justify-content-between">
                          <img src={item.img} alt="1" />
                          <p>{item.text}</p>
                        </div>
                        <button onClick={() => toggle(item)}>Try on!</button>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
            <div
              class="tab-pane fade"
              id="pills-contact2"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <h3>Your Trousers sizes</h3>
            </div>
            <div
              class="tab-pane fade"
              id="pills-contact3"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <h3>Your Dress sizes</h3>
            </div>
            <div
              class="tab-pane fade"
              id="pills-contact4"
              role="tabpanel"
              aria-labelledby="pills-contact-tab"
            >
              <h3>Your Combo sizes</h3>
            </div>
          </div>
        </div>
      </div>
      <Modal
        size="lg"
        isOpen={modal}
        toggle={() => toggle("mod")}
        className={className}
      >
        <ModalBody>
          <div className="container">
            <div className="tab-brand">
              <Nav tabs>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "6" })}
                    onClick={() => {
                      toggle("6");
                    }}
                  >
                    Skirt
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "7" })}
                    onClick={() => {
                      toggle("7");
                    }}
                  >
                    Shoes
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "8" })}
                    onClick={() => {
                      toggle("8");
                    }}
                  >
                    Shirt
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "9" })}
                    onClick={() => {
                      toggle("9");
                    }}
                  >
                    T-Shirt
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "10" })}
                    onClick={() => {
                      toggle("10");
                    }}
                  >
                    Trousers
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "11" })}
                    onClick={() => {
                      toggle("11");
                    }}
                  >
                    Dress
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink
                    className={classnames({ active: activeTab2 === "12" })}
                    onClick={() => {
                      toggle("12");
                    }}
                  >
                    Combo
                  </NavLink>
                </NavItem>
              </Nav>
              <TabContent activeTab={activeTab2}>
                <TabPane tabId="6">6</TabPane>
                <TabPane tabId="7">7</TabPane>
                <TabPane tabId="8">8</TabPane>
                <TabPane tabId="9">
                  <div className="row">
                    <div className="col-lg-6 text-center">
                      <h4>
                        Try on different sizes and see how it looks on you
                      </h4>
                      <div className="text-center">
                        <img
                          className="brand-img"
                          src={activeElement.img}
                          alt="1"
                        />
                      </div>
                      <div className="tablar">
                        <Nav className="nav12" tabs>
                          <NavItem>
                            <NavLink
                              className={`${classnames({
                                active: activeTab === "1",
                              })}`}
                              onClick={() => {
                                toggle("1");
                              }}
                            >
                              XS
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: activeTab === "2",
                              })}
                              onClick={() => {
                                toggle("2");
                              }}
                            >
                              S
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: activeTab === "3",
                              })}
                              onClick={() => {
                                toggle("3");
                              }}
                            >
                              M
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: activeTab === "4",
                              })}
                              onClick={() => {
                                toggle("4");
                              }}
                            >
                              L
                            </NavLink>
                          </NavItem>
                          <NavItem>
                            <NavLink
                              className={classnames({
                                active: activeTab === "5",
                              })}
                              onClick={() => {
                                toggle("5");
                              }}
                            >
                              XL
                            </NavLink>
                          </NavItem>
                        </Nav>
                      </div>
                      <button
                        onClick={() => toggle("mod")}
                        className="btn btn-primary mb-3"
                      >
                        <span> Save</span> <br /> and return to brand selection
                      </button>
                    </div>
                    <div className="col-lg-6 text-center">
                      <TabContent activeTab={activeTab}>
                        <TabPane tabId="1">1 </TabPane>
                        <TabPane tabId="2">
                          <img src="/images/brands/size/Group1.png" alt="" />
                        </TabPane>
                        <TabPane tabId="3">
                          <img src="/images/brands/size/Group.png" alt="" />
                        </TabPane>
                        <TabPane tabId="4">
                          <img src="/images/brands/size/Group2.png" alt="" />
                        </TabPane>
                        <TabPane tabId="5">5</TabPane>
                      </TabContent>
                    </div>
                  </div>
                </TabPane>
                <TabPane tabId="10">10</TabPane>
                <TabPane tabId="11">11</TabPane>
                <TabPane tabId="12">12</TabPane>
              </TabContent>
            </div>
          </div>
        </ModalBody>
      </Modal>
    </>
  );
}
